require 'test_helper'
require 'net/http'

class HomeControllerTest < ActionController::TestCase
  test "foursquare_redirect" do
    uri = URI("http://foursquare.com/oauth2/authenticate?client_id=#{Rails.application.secrets['client_id']}&response_type=code&redirect_uri=http://localhost:3000/users/callback")
    res = Net::HTTP.get_response(uri)

    assert_equal("301", res.code)
  end
end
