require 'net/http'

class Foursquare
  private
  def self.user_friends_checkins(resp, access_token)
    @checkins = {}
    resp["response"]["recent"].each do |r|
      @checkins[r["id"]] = { user: "#{r["user"]["firstName"]} #{r["user"]["lastName"]}",
                             user_photo: "#{r["user"]["photo"]["prefix"]}300x300#{r["user"]["photo"]["suffix"]}",
                             venue_id: r["venue"]["id"],
                             venue_name: r["venue"]["name"],
                             venue_photo: get_venue_picture(r["venue"]["id"], access_token)}
    end
    @checkins
  end

  def self.get_venue_picture(venue_id, access_token)
    resp = json_parse("https://api.foursquare.com/v2/venues/#{venue_id}/photos?oauth_token=#{access_token}&v=20140526")

    if resp["response"]["photos"]["items"].count > 0
      "#{resp["response"]["photos"]["items"][0]["prefix"]}300x300#{resp["response"]["photos"]["items"][0]["suffix"]}"
    else
      "http://placehold.it/200x200"
    end
  end

  def self.get_response(url)
    uri = URI(url)
    Net::HTTP.get(uri)
  end

  def self.json_parse(resp)
    JSON.parse(get_response(resp))
  end
end
