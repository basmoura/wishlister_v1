class UserSessionsController < ApplicationController
  def callback
    res = Foursquare.get_response("https://foursquare.com/oauth2/access_token?client_id=#{Rails.application.secrets['client_id']}&client_secret=#{Rails.application.secrets['client_secret']}&grant_type=authorization_code&redirect_uri=http://localhost:3000/user_sessions/callback&code=#{params[:code]}")
    resp = Foursquare.json_parse("https://api.foursquare.com/v2/users/self?oauth_token=#{get_token(res)}&v=20140525")

    @user = User.where(access_token: get_token(res)).take

    if @user
      authenticate @user
    else
      create(resp, get_token(res))
    end
  end

  def create(resp, token)
    @user = User.new
    @user.name = resp["response"]["user"]["firstName"]
    @user.last_name = resp["response"]["user"]["lastName"]
    @user.access_token = token
    @user.photo_url = "#{resp["response"]["user"]["photo"]["prefix"]}300x300#{resp["response"]["user"]["photo"]["suffix"]}"
    @user.save
    authenticate @user
  end

  def authenticate(user)
    session[:user_id] = user.id
    current_user
    redirect_to wishlist_path(user.id)
    flash[:success] = t("session.signin")
  end

  def destroy
    session[:user_id] = nil
    Rails.cache.delete("checkins")
    redirect_to root_path
    flash[:success] = t("session.signout")
  end

  private
  def get_token(value)
    access_token = value.split(/:/)
    access_token[1].gsub(/["}]/,"")
  end
end
