class UsersController < ApplicationController
  def show
    if current_user
      @checkins ||= get_checkins
      @wishes = User.find(current_user.id).wishes
    else
      redirect_to root_path
    end
  end

  private
  def get_checkins
    resp = Foursquare.json_parse("https://api.foursquare.com/v2/checkins/recent?oauth_token=#{current_user.access_token}&limit=10&v=20140526")
    Rails.cache.fetch("checkins", expires_in: 10.minutes) { Foursquare.user_friends_checkins(resp, current_user.access_token) }
  end
end
