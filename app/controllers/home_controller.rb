class HomeController < ApplicationController
  def index
    redirect_to wishlist_path(current_user.id) if current_user
  end
end
