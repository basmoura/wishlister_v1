class WishesController < ApplicationController
  def create
    @wish = Wish.new(wish_params)
    if @wish.save
      redirect_to wishlist_path(current_user.id)
      flash[:success] = t("wish.add")
    end
  end

  def destroy
    @wish = Wish.find(params[:id])
    @wish.destroy
    redirect_to wishlist_path(current_user.id)
    flash[:success] = t("wish.del")
  end

  private
  def wish_params
    params.require(:wish).permit(:venue_name, :venue_photo, :user_id)
  end
end
