Rails.application.routes.draw do
  resources :users, except: [:show]
  resources :wishes
  resource :user_sessions, only: [:destroy]

  root "home#index"
  get "/user_sessions/callback", to: "user_sessions#callback"
  get "user/:id/wishlist", to: "users#show", as: "wishlist"
end
